-- drop table if exists flights;
create table flights
(
    fid  serial primary key,     -- flight id
    year int,                    -- departure year
    month int,                   -- departure month
    day_of_month int,            -- departure day
    day_of_week int,             -- depature day (maps to dayofweek.did)
    fl_date varchar(10),         -- departure date YYYY-MM-DD
    carrier varchar(3),          -- carrier code (maps to carriers.cid)
    fl_num varchar(4),           -- flight number
    origin varchar(3),           -- origin code (maps to airports.aid)
    origin_city_name varchar(255), -- origin city name and state
    origin_state_abr varchar(2), -- origin state abbreviation
    dest varchar(3),             -- destination code (maps to airports.aid)
    dest_city_name varchar(255), -- destination city name and state
    dest_state_abr varchar(2),   -- destination state abbreviation
    crs_dep_time varchar(4), -- scheduled departure time, local time, based on Computerized Reservations Systems (CRS)
    dep_time varchar(4),         -- actual depature time, local time
    crs_arr_time varchar(4), -- scheduled arrival time, local time, based on CRS
    arr_time varchar(4),         -- actual arrival time, locat time
    cancelled float,             -- 1=yes, 0=no
    cancellation_code varchar(4), -- A=Carrier, B=Weather, C=National Air System, D=Security
    diverted float               -- 1=yes, 0=no
);

-- drop table if exists carrier;
create table carriers
(
   cid varchar(3),
   description varchar(255)
);

-- drop table if exists airports;
create table airports
(
    aid varchar(3),
    description varchar(255)
);

create table months
(
    mid int,
    description varchar(255)
);

create table dayofweek
(
    did int,
    description varchar(255)
);

