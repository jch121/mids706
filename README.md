### Overview
Sample files scripts for MIDS-706, Data Management.

## SQL
The directory SQL contains scripts and data for sample SQL problems.

# MIDS RDB Flights dataset
Sample files for Relational Database lectures and Intro to SQL. Includes table creation script (create-ontime.sql). The file `ontime_public.tar.gz` is an archive containing data for 5 tables:
* airports: 3 char airport code and description
* carriers: 2-3 char carrier code and description
* dayofweek: day of week, 1=Mon, 7=Sun
* flights: main file, contains scheduled and actual departure and arrival times, US, Nov 2018
* months.csv: int-to-month mapping, 1=Jan, 12=Dec
